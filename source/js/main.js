$(window).on("load",function(){
    $(".mapcenter__list--js").mCustomScrollbar();
});

$(document).ready(function () {


  // anchor
  $(".anchor").on("click","a", function (event) {
      event.preventDefault();
      var id  = $(this).attr('href'),
          top = $(id).offset().top;
      $('body,html').animate({scrollTop: top}, 1000);
  });
  // anchor

  //genplan
  function addText(p, text)
  {
      var t = document.createElementNS("http://www.w3.org/2000/svg", "text");
      var b = p.getBBox();
      t.setAttribute("transform", "translate(" + (b.x + b.width/2 -10) + " " + (b.y + b.height/2 + 5) + ")");
      t.textContent = text;
      t.setAttribute("fill", "white");
      t.setAttribute("font-size", "14");
      p.parentNode.insertBefore(t, p.nextSibling);
  }
  function infoPanel(sectionMain,sectionSecond,title,logo) {
    var infoSectionMain = $('.svg-info-sectionMain');
    var infoSectionSecond = $('.svg-info-sectionSecond');
    var infoTitle = $('.svg-info-title');
    var infoLogo = $('.svg-info-logo');
    infoSectionMain.html("");
    infoSectionSecond.html("");
    infoTitle.html("");
    infoLogo.html("");
    if(infoSectionMain) {
      infoSectionMain.html("<button class='form__submit'>"+sectionMain+"</button>");
    }
    if(sectionSecond) {
      infoSectionSecond.html("<button class='form__submit form__submit--outline'>"+sectionSecond+"</button>");
    }
    if(infoTitle) {
      infoTitle.html(title);
    }
    if(infoLogo) {
      infoLogo.html("<img src="+logo+" alt="+title+">");
    }
  }

  $('.mapcenter__list--js li').click(function () {
    $('text').remove();
    $('.genplan-path').removeClass('active');
    $('.mapcenter__list--js li').removeClass('active');
    $(this).addClass('active');
    var lid = $(this).data('lid');
    var paths = $('.genplan-path[data-id = '+lid+']');
    var label = paths.data('label');
    var title = paths.data('title');
    var logo = paths.data('logo');
    var sectionMain = paths.data('section');
    var sectionSecond = paths.data('section2');
    paths.addClass('active');
    infoPanel(sectionMain,sectionSecond,title,logo);
    if(lid < '100') {
      $('.tabs__caption li').removeClass("active");
      $('.tabs__content').removeClass("active");
      $('.tabs__content:eq(0)').addClass("active");
      $('.tabs__caption li:eq(0)').addClass("active");
    }
    if(lid > '100' && lid < '200') {
      $('.tabs__caption li').removeClass("active");
      $('.tabs__content').removeClass("active");
      $('.tabs__content:eq(1)').addClass("active");
      $('.tabs__caption li:eq(1)').addClass("active");
    }
    if(lid > '200') {
      $('.tabs__caption li').removeClass("active");
      $('.tabs__content').removeClass("active");
      $('.tabs__content:eq(2)').addClass("active");
      $('.tabs__caption li:eq(2)').addClass("active");
    }
    addText(paths[0], label);
  });
  $('.path-group').click(function () {
    $('text').remove();
    $('.genplan-path').removeClass('active');
    $('.mapcenter__list--js li').removeClass('active');

    var paths = $(this).find('.genplan-path');
    //data attr
    var label = paths.data('label');
    var id = paths.data('id');
    var title = paths.data('title');
    var logo = paths.data('logo');
    var sectionMain = paths.data('section');
    var sectionSecond = paths.data('section2');
    //data attr
    var scrollPos = $('.mapcenter__list--js li[data-lid = '+id+']').position().top;
    paths.addClass('active');
    $('.mapcenter__list--js li[data-lid = '+id+']').addClass('active');
    $(".mapcenter__list--js").mCustomScrollbar("scrollTo", scrollPos);
    infoPanel(sectionMain,sectionSecond,title,logo);

    // console.log(scrollPos);
    // if(scrollPos != 0) {
    //   $('.mapcenter__list--js').animate({
    //       scrollTop: scrollPos
    //   },500);
    // }

    addText(paths[0], label);
  });
  $('.path-group').mouseover(function (e) {
    var store = $(this).find('.genplan-path').data('store');
    if (typeof store !== typeof undefined && store !== false) {
      $('.popover').addClass('active').html('<img src="'+store+'" />');
    }
  }).mouseleave(function () {
      $('.popover').removeClass('active')
  }).mousemove(function(e) {
      var mouseX = e.pageX,
          mouseY = e.pageY;
      $('.popover').css({
          top: mouseY-100,
          left: mouseX - ($('.popover').width()/2)
      });
  });
  //genplan

  //init 3d slider
	$('.main').cubes();
  //init 3d slider

  //adaptive menu
  $('.toggle--js').click(function () {
    $(this).next().slideToggle();
  });
  //adaptive menu

  //showmore
  $('.showmore--js').click(function () {
    $(this).hide().parent().next().slideDown();
    return false;
  });
  $('.show-phone--js').click(function () {
    $(this).html($(this).data('phone'));
    return false;
  });
  //showmore

  // slider
  var slideshow = $('.slideshow');
  var carousel = $('.carousel');

  carousel.slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    responsive: [
      {
        breakpoint: 1700,
        settings: {
          variableWidth: true,
          centerMode: true,
          centerPadding: '0',
          dots: true,
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: 1467,
        settings: {
          variableWidth: true,
          centerMode: true,
          centerPadding: '0',
          dots: true,
          slidesToShow: 4,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 1125,
        settings: {
          variableWidth: true,
          centerMode: true,
          centerPadding: '0',
          dots: true,
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 869,
        settings: {
          variableWidth: true,
          centerMode: true,
          centerPadding: '0',
          dots: true,
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 580,
        settings: {
          variableWidth: true,
          centerMode: true,
          centerPadding: '0',
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  slideshow.slick({
    dots: true,
    infinite: true,
    arrows: false,
    adaptiveHeight: true
  });

  (function () {
      var $frame = $('.slider');

      $frame.sly({
          slidee: '.slider__items',
          horizontal: true,
          itemNav: 'basic',
          smart: true,
          activateOn: 'click',
          mouseDragging: false,
          touchDragging: true,
          releaseSwing: 1,
          startAt: 0,
          scrollBar: $frame.find('.slider__controls'),
          speed: 300,
          elasticBounds: 1,
          easing: 'easeOutExpo',
          dragHandle: 1,
          dynamicHandle: false,
          // Buttons
          prev: $frame.find('.slider__btn--prev'),
          next: $frame.find('.slider__btn--next')
      }).init();

      $(window).resize(function(e) {
        $frame.sly('reload');
      });
  })();
  // slider

  // tabs
  $(function() {
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
      $('.popover, .popover-txt').removeClass('active')
      $(this)
        .addClass('active').siblings().removeClass('active')
        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
  });
  // tabs

  //panel
  $(function() {
    $('.panel--collapsed').each(function(index, elem) {
      $(elem).find('.panel__more').on('click', function() {
        $(elem).toggleClass('panel--expand');
        $(elem).find('.panel__items--extra').stop().slideToggle(200);
      });
    });
  });
  //panel

  //group
  $(function() {
    $('.group').each(function(index, elem) {
      $(elem).find('.group__btn').on('click', function() {
        if ($(elem).hasClass('group--collapsed')) {
          $(elem).find('.group__content').stop().slideDown(200, function() {
            $(elem).removeClass('group--collapsed');
          });
        } else {
          $(elem).find('.group__content').stop().slideUp(200, function() {
            $(elem).addClass('group--collapsed');
          });
        }
      });
    });
  });
  //group


  //add char
  $('.form__btn--js').click(function (){
    $(".form__items--container").append(
      '<li class="form__item"><input class="form__input form__input--lg form__input--sub" placeholder="Название" type="text"><input class="form__input form__input--lg" type="text" placeholder="Свойство"></li>'
    );
  });
  //add char

  //readonly input
  $(function() {
    $('.form__input.form__input--readonly').each(function(index, elem) {
      $(elem).find('.form__btn--edit').on('click', function() {
        $(elem).css("display", "none");
        $(elem).next(".form__input").css("display", "block").focus();
      });
    });
  });
  //readonly input

  // custom select
  $(function() {
    $('select').selectric().selectricPlaceholder({placeholderOnOpen: false});
  });
  // custom select
});
