'use strict';

//created by NovaVovikov https://github.com/novavovikov

//cubes plugin


/*

__________________________________________________



Created by NovaVovikov



github: https://github.com/novavovikov/

e-mail: novavovikov@gmail.com

__________________________________________________



*/

(function ($) {

	var defaults = {

		banner: '.banner-main',

		imgWrap: '.banner__img',

		title: '.banner__title',

		subtitle: '.banner__subtitle',

		controls: '[data-control]',

		activeClass: 'active',

		autoplay: true,

		autoplayTimeout: 3000,

		autoplayStartPosition: 0

	};

	$.fn.cubes = function (options) {

		if (this.length == 0) return this;

		if (this.length > 1) {

			this.each(function () {
				$(this).cubes(options);
			});

			return this;
		}

		var node = {},
		    data = {},
		    el = this;

		var settings = $.extend({}, defaults, options);

		var methods = {

			init: function init() {

				node.wrap = el;

				node.banner = node.wrap.find(settings.banner);

				node.imgWrap = node.wrap.find(settings.imgWrap);

				node.img = node.imgWrap.find('img');

				node.title = node.wrap.find(settings.title);

				node.subtitle = node.wrap.find(settings.subtitle);

				node.controls = node.wrap.find(settings.controls);

				node.controls.hover(methods.startControlHover, methods.endHover);

				node.banner.hover(methods.startBannerlHover, methods.endHover);

				settings.autoplay ? methods.autoPlay() : '';
			},

			runAutoplay: function runAutoplay() {

				data.timer = setTimeout(function tick() {

					var index = data.currIndex + 1;

					methods.checkPosition(index);

					node.activeItem = node.controls.eq(data.currIndex);

					methods.changeBanner();

					data.timer = setTimeout(tick, settings.autoplayTimeout);
				}, settings.autoplayTimeout);
			},

			autoPlay: function autoPlay() {

				if (data.currIndex === undefined) {

					data.currIndex = settings.autoplayStartPosition;

					node.activeItem = node.controls.eq(data.currIndex);

					methods.changeBanner();
				}

				methods.runAutoplay();
			},

			checkPosition: function checkPosition(index) {

				if (index >= 0 && index < node.controls.length) {

					data.currIndex = index;

					data.prevIndex = data.currIndex - 1;

					data.nextIndex = data.currIndex + 1;

					data.prevIndex < 0 ? data.prevIndex = node.controls.length - 1 : '';

					data.nextIndex >= node.controls.length ? data.nextIndex = 0 : '';
				} else if (index < 0) {

					data.currIndex = node.controls.length - 1;

					data.prevIndex = 0;

					data.nextIndex = node.controls.length - 2;
				} else if (index >= node.controls.length) {

					data.currIndex = 0;

					data.prevIndex = node.controls.length - 1;

					data.nextIndex = 1;
				}
			},

			startControlHover: function startControlHover() {

				node.activeItem = $(this);

				clearTimeout(data.timer);

				methods.changeBanner();
			},

			startBannerlHover: function startBannerlHover() {

				clearTimeout(data.timer);
			},

			endHover: function endHover() {

				data.currIndex = node.activeItem.index();

				methods.runAutoplay();
			},

			changeBanner: function changeBanner() {

				data.activeClassName = '_' + node.activeItem.data('name');

				data.img = node.activeItem.data('img');

				data.title = node.activeItem.data('title');

				data.subtitle = node.activeItem.data('subtitle');

				for (var i = 0; i < node.controls.length; i++) {

					var className = '_' + node.controls[i].getAttribute('data-name');

					node.banner.removeClass(className);

					node.imgWrap.removeClass(className);
				}

				node.controls.removeClass(settings.activeClass);

				node.activeItem.addClass(settings.activeClass);

				node.banner.addClass(data.activeClassName);

				node.imgWrap.addClass(data.activeClassName);

				node.title.text(data.title);

				node.subtitle.text(data.subtitle);

				node.img.attr('src', data.img);
			}

		};

		methods.init();
	};
})(jQuery);
