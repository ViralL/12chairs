'use strict';

//parallax plugin

/*
__________________________________________________

Created by NovaVovikov

github: https://github.com/novavovikov/
e-mail: novavovikov@gmail.com
__________________________________________________

*/

(function($) {
    var defaults = {
        ratioX: 0.03,
        ratioY: 0.03,
        xScroll: true,
        yScroll: true,
        xInvert: false,
        yInvert: false,
        responsive: true
    };

    $.fn.parallax = function(options) {
        if (this.length == 0) return this;

        if (this.length > 1) {
            this.each(function() {
                $(this).parallax(options);
            });
            return this;
        }

        var parallax = {},
            data = {},
            el = this;
        var settings = $.extend({}, defaults, options);

        var methods = {
            init: function init() {
                parallax.obj = el;

                //preset parametrs

                settings.x0 = parseInt(parallax.obj.css('left'));
                settings.y0 = parseInt(parallax.obj.css('top'));

                settings.windowY = $(window).height() / 2;
                settings.windowX = $(window).width() / 2;

                //responsive
                if (settings.responsive == true) {
                    methods.responsive();
                }

                //scroll watching
                if (settings.xScroll == true) {
                    $(window).mousemove(methods.xScroll);
                } else if (settings.yScroll == true) {
                    $(window).mousemove(methods.yScroll);
                }
            },
            xScroll: function xScroll(e) {
                if (settings.xInvert == true) {
                    settings.x = settings.x0 - (e.screenX - settings.windowX) * settings.ratioX;
                    parallax.obj.css('left', settings.x);
                } else {
                    settings.x = settings.x0 + (e.screenX - settings.windowX) * settings.ratioX;
                    parallax.obj.css('left', settings.x);
                }

                if (settings.yScroll == true) methods.yScroll(e);
            },
            yScroll: function yScroll(e) {
                settings.y = settings.y0 + (e.screenY - settings.windowY) * settings.ratioY;
                parallax.obj.css('top', settings.y);
            },
            responsive: function responsive() {
                $(window).resize(function() {
                    settings.windowY = $(window).height() / 2;
                    settings.windowX = $(window).width() / 2;

                    parallax.obj.css('left', settings.x0);
                    parallax.obj.css('top', settings.y0);
                });
            }

        };

        methods.init();
    };
})(jQuery);
