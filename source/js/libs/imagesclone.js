
if($('.banner-wrap').length) {
  var image_clone;
  (function() {
    var preloader = document.querySelectorAll('[data-loader]'),
      images = document.images,
      images_total_count = images.length,
      images_loaded_count = 0,
      steps = document.querySelectorAll('.cube._chair');

    for (var i = 0; i < images_total_count; i++) {
      image_clone = new Image();
      image_clone.onload = image_loaded;
      image_clone.onerror = image_loaded;
      image_clone.src = images[i].src;
    }

    function image_loaded() {
      images_loaded_count++;
      var percents = ((100/images_total_count) * images_loaded_count) << 0;

      if (percents > 33.33339) steps[0].classList.add('active');
      if (percents > 66.6666) steps[1].classList.add('active');
      if (percents > 99.999) steps[2].classList.add('active');

      if (images_loaded_count >= images_total_count) {
        setTimeout(function() {
          for (var i = 0; i < preloader.length; i++) {
            if (preloader[i].getAttribute('data-loader') === 'overlay') {
              var overlay = preloader[i];
              preloader[i].addEventListener('transitionend', function() {
                overlay.remove();
              });
            }
            preloader[i].classList.add('active');
          }

          setTimeout(function() {
            $('.cube._kitchens').parallax();
            $('.cube._actions').parallax({
              xInvert: true
            });
            $('.cube._cabinets').parallax({
              yScroll: true
            });
            $('.cube._childrens').parallax();
            $('.cube._sofa').parallax({
              xInvert: true
            });

            $('.cube._bedrooms').parallax({
              yInvert: true
            });
          }, 1300);
        }, 500);
      }
    }
  })();
}
