"use strict";

const gulp = require("gulp"),
    plumber = require("gulp-plumber"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
// it adds browser prefixes
    autoprefixer = require("autoprefixer"),
// it reorders css options in wished order
    csscomb = require("gulp-csscomb"),
// css minifier
    nano = require("gulp-cssnano"),
// it helps rename files/
// for example, adding suffix .min to files
    rename = require("gulp-rename"),
// image minifier
    imagemin = require("gulp-imagemin"),
// svg minifier
    svgmin = require("gulp-svgmin"),
// svg sprite
    svgstore = require("gulp-svgstore"),
// create png icon's sprite
    spritesmith = require('gulp.spritesmith'),
// resolve image paths
    assets = require("postcss-assets"),
// js transpaler
    babel = require("rollup-plugin-babel"),
// js minifier
    uglify = require("gulp-uglify"),
// js modules builder
    rollup = require("gulp-better-rollup"),
// js sourcemaps
    sourcemaps = require("gulp-sourcemaps"),
// HTML templates engin
    nunjucksRender = require("gulp-nunjucks-render"),
// json data for templates
    data = require("gulp-data"),
// it replaces one string in the file to another.
    replace = require("gulp-replace"),
// it hepls to execute tasks in the right order
    runSequence = require("run-sequence"),
// livereload
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
// it organize media querries at the end of the style file
    combineMq = require("gulp-combine-mq"),
// it removes files
    del = require("del");


var path = {
  build: {
    css: "dist/css",
    html: "dist",
    fonts: "dist/fonts",
    js: "dist/js",
    img: "dist/img",
    svg: "dist/img/svg"
  },
  src: {
    self: "source",
    style: {
      temp: "source/sass/*.scss",
      folder: "source/sass/**/**.*",
      self: "source/sass/style.scss"
    },
    html: {
      templates: "source/templates",
      folder: "source/**/*.html",
      self: "source/*.html"
    },
    fonts: "source/fonts/**/*.*",
    img: [
      "source/img/**/*.*",
      "!source/img/sprite-icons/**/*.*",
      "!source/img/inline-icons/**/*.*",
      "!source/img/svg/symbols/**/*.*"
    ],
    svg: [
      "source/img/svg/**/*.*",
      "!source/img/sprite-icons/**/*.*",
      "!source/img/inline-icons/**/*.*",
      "!source/img/svg/symbols/**/*.*"
    ],
    symbols: {
      source: "source/img/svg/symbols/**/*.*",
      dest: "source/img/svg/"
    },
    sprite: {
      source: "source/img/sprite-icons/**/*.png",
      img: "source/img/",
      css: "source/sass/global/"
    },
    inline: {
      source: "source/img/inline-icons"
    },
    script: {
      folder: "source/js/**/**.js",
      self: "source/js/main.js",
      jquery: "source/js/libs/jquery/*.js",
      libs: "source/js/libs/*.js"
    }
  }
};


gulp.task("html", function() {
  return gulp.src(path.src.html.self)
    .pipe(data(function() {
      return require("./source/templates/pages-data.json");
    }))
    .pipe(nunjucksRender({
      path: [path.src.self]
    }))
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({
      stream: true
    }));
});

gulp.task("build:html", function() {
  return gulp.src([path.src.html.self])
    .pipe(data(function() {
      return require("./source/templates/pages-data.json");
    }))
    .pipe(nunjucksRender({
      path: [path.src.self]
    }))
    // .pipe(replace(/.css\"/g, ".min.css\""))
    // .pipe(replace(/.js\"/g, ".min.js\""))
    .pipe(gulp.dest(path.build.html));
});

gulp.task("style", function() {
  return gulp.src([path.src.style.self])
    .pipe(plumber())
    .pipe(sass({
        outputStyle: "expanded"
      }).on("error", sass.logError))
    .pipe(postcss([
        assets({
          loadPaths: [path.build.img, path.build.svg, path.src.inline.source],
          relativeTo: path.build.css
        }),
        autoprefixer({ browsers: ["last 4 versions"] })
      ]))
    .pipe(csscomb())
    .pipe(combineMq())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({
      stream: true
    }));
});

gulp.task("build:style", function() {
  return gulp.src([path.src.style.self])
    .pipe(sass({
        outputStyle: "expanded"
      }).on("error", sass.logError))
    .pipe(postcss([
        assets({
          loadPaths: [path.build.img, path.build.svg, path.src.inline.source],
          relativeTo: path.build.css
        }),
        autoprefixer({ browsers: ["last 4 versions"] })
      ]))
    .pipe(csscomb())
    .pipe(combineMq())
    .pipe(gulp.dest(path.build.css))
    // .pipe(nano())
    // .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(path.build.css));
});

gulp.task("fonts", function() {
  return gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts));
});

gulp.task("img", function() {
  return gulp.src(path.src.img)
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({
      stream: true
    }));
});

gulp.task("svg", function() {
  return gulp.src(path.src.svg)
    .pipe(svgmin())
    .pipe(gulp.dest(path.build.svg));
});

gulp.task("symbols", function() {
  return gulp.src(path.src.symbols.source)
    .pipe(svgmin())
    .pipe(svgstore({
      inlineSvg : true
    }))
    .pipe(rename("symbols.svg"))
    .pipe(gulp.dest(path.src.symbols.dest));
});

gulp.task("sprite", function() {
  const spriteData = gulp.src(path.src.sprite.source)
    .pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: '_sprite.scss'
    }));

    spriteData.img.pipe(gulp.dest(path.src.sprite.img));
    spriteData.css.pipe(gulp.dest(path.src.sprite.css));
});

gulp.task("build:img", function() {
  return gulp.src(path.src.img)
    .pipe(imagemin({
      progressive: true,
      optimizationLevel: 3,
      svgoPlugins: [{ cleanupIDs: false }],
      multipass: true
    }))
    .pipe(gulp.dest(path.build.img));
});

gulp.task("build:js", function() {
  return gulp.src([path.src.script.jquery,
      path.src.script.libs,
      path.src.script.self
    ])
    //.pipe(rollup({
        //plugins: [babel({exclude: 'node_modules/**'})]
    //  }, 'iife'))
    // .pipe(rename({ suffix: ".min" }))
    // .pipe(uglify())
    .pipe(gulp.dest(path.build.js));
});

gulp.task("js", function() {
  return gulp.src([path.src.script.jquery,
      path.src.script.libs,
      path.src.script.self
    ])
    .pipe(plumber())
    //.pipe(sourcemaps.init())
    //.pipe(rollup({
    //    plugins: [babel({exclude: 'node_modules/**'})]
    //  }, 'iife'))
    //.pipe(sourcemaps.write(''))
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({
      stream: true
    }));;
});

gulp.task("clean", function() {
  return del([path.build.html]);
});


gulp.task("server", function() {
  browserSync({
    server: {
      baseDir: "./dist/"
    },
    logLevel: "info",
    open: false,
    ghostMode: false,
    notify: false,
    injectChanges: true,
    codeSync: true,
    reloadDelay: 500
  });
});

gulp.task("watch", function() {
  gulp.watch(path.src.style.folder, ["style"]);
  gulp.watch(path.src.html.folder, ["html"]);
  gulp.watch(path.src.fonts, ["fonts"]);
  gulp.watch(path.src.img, ["img"]);
  gulp.watch(path.src.script.folder, ["js"]);
});

gulp.task("build", function() {
  runSequence("clean", "svg", "symbols", "build:img", "fonts", "build:style", "build:js", "build:html");
});

gulp.task("default", function() {
  runSequence("clean", "symbols", "img", "fonts", "style", "js", "html", "server", "watch");
});
